<?php
/**
 * The base configurations of WordPress.

 * This file has been modified to allows specific configurations for 3 different 
 * development environments (local, staging, live). Depending on the server WordPress
 * resides on it will use the approriate settings. Just fill oyt the variables below 
 * to get it going.
 
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 
ADAPTED FROM: 

*  David Bainbridge:
*  http://www.codesynthesis.co.uk/tutorials/creating-config-files-for-multiple-environments-in-wordpress

*  @franz-josef-kaiser
*  https://gist.github.com/franz-josef-kaiser/859325

@package WordPress
*/

/*-------------------------------\
\\   __    __   ____    _____   //
//   | \  / |   |   \   ||--/   \\
\\   ||\\//||   | |) |  ||--/   //
//   || \/ ||   |   /   ||      \\
\\   --    --.  ---'.   -- .    //
//                              \\
\-------------------------------*/

/*************************************************************************\
|  Environment Variables                                                  |
\*************************************************************************/

//Define local constants
    $local_IP = ""; //IP of local machine with DB
    $local_directory = ""; //directory name only
    
//Define DB's
    $local_db = "";
    $stage_db = "";
    $live_db = "";

//Define Server Environments - leave out "http://"
    //local defaults to localhost
    $stage_server = "";
    $live_server = "";

//Define URLs - INCLUDE! "http://" - include trailing slash
    //local is set to http:// + local IP + local Directory
    $stage_url = "";
    $live_url = "";
    
//Define logins
    $stage_user = "";
    $stage_pass = "";
    $live_user = "";
    $live_pass = "";

//WordPress Table Prefix
    $table_prefix  = 'wp_';

//WordPress Root Folder name    
    $root = "_system";


/*************************************************************************\
|  Local, Staging, Live settings                                          |
\*************************************************************************/

$local_url = 'http://' . $local_IP . "/" . $local_directory ."/";
$local_site = $local_url . $root;
$stage_site = $stage_url . $root;
$live_site = $live_url . $root;
$non_www = substr_replace( $live_server, "", 0, 4 );

switch($_SERVER['SERVER_NAME']) {
    
//LOCAL
    case "$local_IP":
	// DB
	   define( 'DB_NAME',	"$local_db" );
	   define( 'DB_USER',	'root' );
	   define( 'DB_PASSWORD', 'root' );
	   define( 'DB_HOST',	'localhost' );
	   
    // URL Constants
	   define( 'WP_SITEURL',"$local_site" );
	   define( 'WP_HOME',	"$local_url" );
	   
	   define( 'AUTOSAVE_INTERVAL',	3600 ); // autosave 1x per hour
	   define( 'EMPTY_TRASH_DAYS',	0 ); // zero days
	   define( 'WP_POST_REVISIONS',	false ); // no revisions
    
	   // DEBUG
	   #error_reporting( E_ALL );
	   define( 'WP_DEBUG',		true );
	   define( 'SAVEQUERIES',	true );
	   define( 'WP_DEBUG_LOG',	true ); // file: /core_root/wp-content/debug.log
	   define( 'WP_DEBUG_DISPLAY', 	false );
	   @ini_set( 'display_errors',	0 );
	   
	   // ALLOW updates without FTP
	   define ( 'FS_METHOD', 'direct' );
    
    // Allows database to be repaired
       define('WP_ALLOW_REPAIR', true);
    break;

//STAGING
    case "$stage_server":
       define( 'DB_NAME',		"$stage_db" );
       define( 'DB_USER',		"$stage_user" );
       define( 'DB_PASSWORD',	"$stage_pass" );
	   define( 'DB_HOST',	'localhost' );
    
	   define( 'WP_SITEURL',	"$stage_site" );
	   define( 'WP_HOME',	"$stage_url" );
	   
	   define( 'AUTOSAVE_INTERVAL',	3600 ); // autosave 1x per hour
	   define( 'EMPTY_TRASH_DAYS',	0 ); // zero days
	   define( 'WP_POST_REVISIONS',	false ); // no revisions
    
	   // DEBUG
	   # error_reporting( E_ALL ); // in case we need it
	   define( 'WP_DEBUG',		true );
	   define( 'SAVEQUERIES',		true );
	   define( 'WP_DEBUG_LOG',		true ); // file: /core_root/wp-content/debug.log
	   define( 'WP_DEBUG_DISPLAY', 	false );
	   @ini_set( 'display_errors', 	0 );

	   // Non-public
	   mysql_query( 'UPDATE wp_options SET option_value = false WHERE option_name = "blog_public"' );
    break;
    
// LIVE
	//apply settings to non www URL also	
	case "$non_www":
    case "$live_server":
       define( 'DB_NAME',		"$live_db" );
       define( 'DB_USER',		"$live_user" );
       define( 'DB_PASSWORD',	"$live_pass" );
	   define( 'DB_HOST',	    'localhost' );
    
	   define( 'WP_SITEURL',	"$live_site" );
	   define( 'WP_HOME',	"$live_url" );
    
	   define( 'AUTOSAVE_INTERVAL',	300 ); // autosave 1x per 5 min
	   define( 'EMPTY_TRASH_DAYS',	7 ); // 30 days
	   define( 'WP_POST_REVISIONS',	3 ); // 5 Revisions per post
    
	   // DEBUG
	   # error_reporting(E_ALL); // in case we need it
	   define( 'WP_DEBUG', false );
       define('DISALLOW_FILE_MODS', true);
       define( 'WP_AUTO_UPDATE_CORE', 'minor' );
    
	   // Public
	   # mysql_query( 'UPDATE wp_options SET option_value = true WHERE option_name = "blog_public"' );
    break;
}


/*************************************************************************\
|  Other global settings                                                  |
\*************************************************************************/

define('DB_CHARSET', 'utf8');
define('DB_COLLATE', '');


/*************************************************************************\
|  SALTS, change to invalidate all existing cookies                       |
|  https://api.wordpress.org/secret-key/1.1/salt/                         | 
\*************************************************************************/

/*SALTS HERE*/

define ('WP_CONTENT_FOLDERNAME', 'files');
define( 'WP_CONTENT_DIR', dirname(__FILE__) . '/files/' );
define('WP_CONTENT_URL', WP_HOME . WP_CONTENT_FOLDERNAME); 

// Absolute path to the WordPress directory.
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

// Sets up WordPress vars and included files.
require_once(ABSPATH . 'wp-settings.php');