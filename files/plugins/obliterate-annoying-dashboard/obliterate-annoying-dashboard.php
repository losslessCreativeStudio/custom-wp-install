<?php
/*
Plugin Name: TEG Obliterate Annoying Dashboard
Description: A light plugin to get rid of the annoying dashboard widgets and create useful one. Must have client image of 320px wide names client_logo.png in the images folder of the your theme.
Version: 1.0
Author: The Eastco Group
Author URI: http://www.theeastcogroup.com
Contributors: Remi Corson
*/

/*----------------------------------------------------*
| Custom WordPress Welcome Widget                     |
*----------------------------------------------------*/

/**
 * Hide default welcome dashboard message and and create a custom one
 *
 * @access      public
 * @since       1.0 
 * @return      void
*/

/*----------------------------------------------------*
| Obliterate Dashboard Widgets                       |
*---------------------------------------------------*/

function remove_dashboard_meta() {
        remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_primary', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
        remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
        remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');
	    remove_action( 'welcome_panel', 'wp_welcome_panel' );
}
add_action( 'admin_init', 'remove_dashboard_meta' );


/*----------------------------------------------------*
| Make Dashboard 1 column                             |
*----------------------------------------------------*/

function wpse126301_dashboard_columns() {
    add_screen_option(
        'layout_columns',
        array(
            'max'     => 2,
            'default' => 1
        )
    );
}
add_action( 'admin_head-index.php', 'wpse126301_dashboard_columns' );


/*----------------------------------------------------*
| Custom Welcome Message                              |
*----------------------------------------------------*/

function rc_my_welcome_panel() {

?>

<style>

.welcome-panel .welcome-panel-column-container {
    clear: both;
    margin: 30px auto;
    overflow: hidden;
    position: relative;
    width: 433px;
}
    
.welcome-panel .welcome-panel-column:first-child {
    width: 52%;
}
    
h1 {
    text-align: center;
}
    
.about-description {
    text-align: center;
}
    
.welcome-panel .welcome-panel-column {
    float: left;
    min-width: 200px;
    width: 48%;
}
    
.videoWrapper iframe {
    height: 100%;
    left: 0;
    position: absolute;
    top: 0;
    width: 100%;
}
    
.videoWrapper {
    height: 0;
    padding-bottom: 56.25%;
    padding-top: 25px;
    position: relative;
    margin-top:30px;
}
    
#client-logo {
    margin:0 auto; 
    display: block;
    margin-bottom:30px;   
}

</style>

<script type="text/javascript">
/* Hide default welcome message */
jQuery(document).ready( function($) 
{
	$('div.welcome-panel-content').hide();
});
</script>

	<div class="custom-welcome-panel-content">
    <img id="client-logo" width="320" class="admin-client" src="<?php echo get_stylesheet_directory_uri(); ?>/images/client_logo.png" />    
	<h1><?php _e( 'Welcome to the WordPress Admin!' ); ?></h1>
	<p class="about-description"><?php _e( 'Here are some beginner tutorials to help you get started with your site:' ); ?></p>
        <div class="videoWrapper" ><iframe src="//www.youtube.com/embed/9S0OTtHYH6w?list=PL0VGOC4NqEG2O7MWsR5WrCUv-A3Awf7Ie" frameborder="0" allowfullscreen></iframe></div>
        <div class="welcome-panel-column-container">
	<div class="welcome-panel-column">
		<h4><?php _e( "Need Some Changes?" ); ?></h4>
        <a href="http://www.theeastcogroup.com/"><img class="center" src="http://www.theeastcogroup.com/files/themes/eastco/images/logos/logo-blue.jpg" alt="The Eastco Group"></a>
		<a class="button button-primary button-hero load-customize hide-if-no-customize" href="mailto:web@theeastcogroup.com?subject=Quote%20Request%20for%20<?php echo get_home_url(); ?>"><?php _e( 'Request a Quote' ); ?></a>
			<p class="hide-if-no-customize"><?php printf( __( 'or, give us a call: 716-662-0536' )); ?></p>
	</div>
	<div class="welcome-panel-column">
		<h4><?php _e( 'Next Steps' ); ?></h4>
		<ul>
		<?php if ( 'page' == get_option( 'show_on_front' ) && ! get_option( 'page_for_posts' ) ) : ?>
			<li><?php printf( '<a href="%s" class="welcome-icon welcome-edit-page">' . __( 'Edit your front page' ) . '</a>', get_edit_post_link( get_option( 'page_on_front' ) ) ); ?></li>
			<li><?php printf( '<a href="%s" class="welcome-icon welcome-add-page">' . __( 'Add additional pages' ) . '</a>', admin_url( 'post-new.php?post_type=page' ) ); ?></li>
		<?php elseif ( 'page' == get_option( 'show_on_front' ) ) : ?>
			<li><?php printf( '<a href="%s" class="welcome-icon welcome-edit-page">' . __( 'Edit your front page' ) . '</a>', get_edit_post_link( get_option( 'page_on_front' ) ) ); ?></li>
			<li><?php printf( '<a href="%s" class="welcome-icon welcome-add-page">' . __( 'Add additional pages' ) . '</a>', admin_url( 'post-new.php?post_type=page' ) ); ?></li>
			<li><?php printf( '<a href="%s" class="welcome-icon welcome-write-blog">' . __( 'Add a blog post' ) . '</a>', admin_url( 'post-new.php' ) ); ?></li>
		<?php else : ?>
			<li><?php printf( '<a href="%s" class="welcome-icon welcome-write-blog">' . __( 'Write your first blog post' ) . '</a>', admin_url( 'post-new.php' ) ); ?></li>
			<li><?php printf( '<a href="%s" class="welcome-icon welcome-add-page">' . __( 'View all pages' ) . '</a>', admin_url( 'edit.php?post_type=page' ) ); ?></li>
		<?php endif; ?>
			<li><?php printf( '<a target="_blank" href="%s" class="welcome-icon welcome-view-site">' . __( 'View your site' ) . '</a>', home_url( '/' ) ); ?></li>
		</ul>
	</div>
	</div>
	</div>

<?php
}

add_action( 'welcome_panel', 'rc_my_welcome_panel' );