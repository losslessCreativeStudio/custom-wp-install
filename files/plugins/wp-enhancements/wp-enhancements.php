<?php

/*
Plugin Name: WordPress Enhancements
Description: A plugin that will add numerous items to WordPress to clean up and extend its power out of the box. This is stuff that should be in core.
Version: 1.4
Author: Miles Fonda
Author URI: http://www.milesdfonda.com
*/

/*
| 
| FEATURES:
| 
|	- Relative Linking in Menus and pages
|   - Support custom post thumbnails
|   - Remove Admin bar options I don't like
|	- Remove Post and Page options hardly used
|   - Allow php in widgets
|   - Dashicons are active on front-facing site
|   - Adds function for Customized Search (<?php search_only($post_type, $search_text); ?>)
|   - Function for Easy Pagination (<?php pagination(); ?>)
|   - Function for Custom Excerpts (<?php custom_excerpt($limit, $key); ?>)
|   - All post types get the "Duplicate" Option. Why isn't this already core???
|   - Enqueue FluidVids so all videos are responsive
|   - Ads a very simple "Maintenance Mode" option in admin bar //IN PROGRESS
|   - Shortcode to add an external file in a page or post
|	- Don't show author usernames
|   - Color Code WordPress tables
|	- Remove Version from header
|	- Force sharper thumbnails
|   - Remove Yoast SEO columns and traffic signal from ever showing up
|	- Allow user to use email for login
|   - Add meta information for Facebook thumbnail to pull from featured images
|   - function to get thumbnail URL
|   - allow upload of SVGs
|
*/

/*----------------------------------------------------*
| RELATIVE LINK SHORTCODE                             |
*----------------------------------------------------*/

//typically for links
function theHomeURL() {
    $homeURL = get_home_url();
    return "$homeURL";
}

add_shortcode('##home', 'theHomeURL');


//typically for file addresses *usually not needed unless in a subdomain*
function theSiteURL() {
    $siteURL = get_site_url();
    return "$siteURL";
}

add_shortcode('##site', 'theSiteURL');


/*----------------------------------------------------*
| THUMBNAIL SUPPORT                                   |
*----------------------------------------------------*/

add_theme_support( 'post-thumbnails' );


/*----------------------------------------------------*
| REMOVE UNUSED ADMIN BAR OPTIONS                     |
*----------------------------------------------------*/

add_action( 'admin_bar_menu', 'remove_wp_logo', 999 );

function remove_wp_logo( $wp_admin_bar ) {
	$wp_admin_bar->remove_node( 'wp-logo' );
	$wp_admin_bar->remove_node( 'comments' );
	$wp_admin_bar->remove_node( 'new-content' );
	$wp_admin_bar->remove_node( 'wpseo-menu' );
}


/*----------------------------------------------------*
| REMOVE CLUTTER FROM POSTS AND PAGES                 |
*----------------------------------------------------*/


add_action('init', 'remove_comment_support', 100);

add_action( 'add_meta_boxes', 'my_add_meta_boxes' );


function remove_comment_support() {
    
	remove_post_type_support( 'page', 'comments' );
	
	remove_post_type_support( 'page', 'trackbacks' );
	
	remove_post_type_support( 'post', 'trackbacks' );
	
	remove_post_type_support( 'page', 'custom-fields' );
	
	remove_post_type_support( 'post', 'custom-fields' );

	}
	

function my_add_meta_boxes() {
	
	//remove_meta_box( 'slugdiv', 'post', 'normal' );
	
	//remove_meta_box( 'slugdiv', 'page', 'normal' );
	
	remove_meta_box( 'authordiv', 'page', 'normal' );
}





/*----------------------------------------------------*
| ALLOW PHP IN WIDGETS                                |
*----------------------------------------------------*/



add_filter('widget_text','execute_php',100);
function execute_php($html){
     if(strpos($html,"<"."?php")!==false){
          ob_start();
          eval("?".">".$html);
          $html=ob_get_contents();
          ob_end_clean();
     }
     return $html;
}


/*----------------------------------------------------*
| ADD DASHICONS ON THE FRONT FACING SITE              |
*----------------------------------------------------*/

function themename_scripts() {
	wp_enqueue_style( 'themename-style', get_stylesheet_uri(), array( 'dashicons' ), '1.0' );
} 
add_action( 'wp_enqueue_scripts', 'themename_scripts' );


/*----------------------------------------------------*
| CUSTOM SEARCH FIELD                                 |
*----------------------------------------------------*/

function search_only($post_type, $search_text = 'Search') {?>
	<form method="get" id="search_form" action="<?php bloginfo('home'); ?>" />
		<input class="searchbar" onfocus="if(this.value == '<?php echo $search_text; ?>') { this.value = ''; }" onblur="if(this.value == '') { this.value = '<?php echo $search_text; ?>'; }" type="text" class="text" name="s" value="<?php echo $search_text; ?>" >	
		<input type="hidden" name="post_type" value="<?php echo $post_type; ?>" />	
		<input type="submit" class="searchSubmit" value=""  />   
	</form>
	<?php }

//put search_only() with parameters in a template to use.





/*----------------------------------------------------*
| PAGINATION                                          |
*----------------------------------------------------*/	

function pagination($pages = '', $range = 4)
{ 
     $showitems = ($range * 2)+1; 
 
     global $paged;
     if(empty($paged)) $paged = 1;
 
     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }  
 
     if(1 != $pages)
     {
         echo "<div class=\"pagination\"><span class=\"pageof\">Page ".$paged." of ".$pages."</span>";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo; First</a>";
         if($paged > 1 && $showitems < $pages) echo "<a class=\"pagePrevious\" href='".get_pagenum_link($paged - 1)."'>&lsaquo; Previous</a>";
 
         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<span class=\"current\">".$i."</span>":"<a href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</a>";
             }
         }
 
         if ($paged < $pages && $showitems < $pages) echo "<a class=\"pageNext\" href=\"".get_pagenum_link($paged + 1)."\">Next &rsaquo;</a>"; 
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a class=\"pageLast\" href='".get_pagenum_link($pages)."'>Last &raquo;</a>";
         echo "</div>\n";
     }
}

//put pagination() on a template to use




/*----------------------------------------------------*
| CUSTOM EXCERPT                                      |
*----------------------------------------------------*/	

function custom_excerpt( $limit = 20, $content_key = '' ) {
		if($content_key != '') {
		$text = get_post_meta( get_the_ID(), $content_key, true); 
		}
		else {
		$text = get_the_excerpt();
		}
		
		$excerpt = explode(' ', $text, $limit);
 		if (count($excerpt)>=$limit) {
 		array_pop($excerpt);
 		$excerpt = implode(" ",$excerpt).'...';
 	} 
 	else {
 		$excerpt = implode(" ",$excerpt);
 	}
 
 	$excerpt = preg_replace('`[[^]]*]`','',$excerpt);
 	return $excerpt;
}

//put echo custom_excerpt(LIMIT, KEY) on a template to use
//can use the key name of the field to get the content from in place of $content_key variable


/*----------------------------------------------------*
| ADD DUPLICATE OPTION TO ALL POST TYPES              |
*----------------------------------------------------*/	



$post_types = get_post_types( '', 'names' ); 

foreach ( $post_types as $post_type ) {
 	add_filter($post_type . '_row_actions', 'rd_duplicate_post_link', 10, 2); 
}

function rd_duplicate_post_as_draft(){
	global $wpdb;
	if (! ( isset( $_GET['post']) || isset( $_POST['post'])  || ( isset($_REQUEST['action']) && 'rd_duplicate_post_as_draft' == $_REQUEST['action'] ) ) ) {
		wp_die('No post to duplicate has been supplied!');
	}
 
	/*
	 * get the original post id
	 */
	$post_id = (isset($_GET['post']) ? $_GET['post'] : $_POST['post']);
	/*
	 * and all the original post data then
	 */
	$post = get_post( $post_id );
 
	/*
	 * if you don't want current user to be the new post author,
	 * then change next couple of lines to this: $new_post_author = $post->post_author;
	 */
	$current_user = wp_get_current_user();
	$new_post_author = $current_user->ID;
 
	/*
	 * if post data exists, create the post duplicate
	 */
	if (isset( $post ) && $post != null) {
 
		/*
		 * new post data array
		 */
		$args = array(
			'comment_status' => $post->comment_status,
			'ping_status'    => $post->ping_status,
			'post_author'    => $new_post_author,
			'post_content'   => $post->post_content,
			'post_excerpt'   => $post->post_excerpt,
			'post_name'      => $post->post_name,
			'post_parent'    => $post->post_parent,
			'post_password'  => $post->post_password,
			'post_status'    => 'draft',
			'post_title'     => $post->post_title,
			'post_type'      => $post->post_type,
			'to_ping'        => $post->to_ping,
			'menu_order'     => $post->menu_order
		);
 
		/*
		 * insert the post by wp_insert_post() function
		 */
		$new_post_id = wp_insert_post( $args );
 
		/*
		 * get all current post terms ad set them to the new post draft
		 */
		$taxonomies = get_object_taxonomies($post->post_type); // returns array of taxonomy names for post type, ex array("category", "post_tag");
		foreach ($taxonomies as $taxonomy) {
			$post_terms = wp_get_object_terms($post_id, $taxonomy, array('fields' => 'slugs'));
			wp_set_object_terms($new_post_id, $post_terms, $taxonomy, false);
		}
 
		/*
		 * duplicate all post meta
		 */
		$post_meta_infos = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id");
		if (count($post_meta_infos)!=0) {
			$sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";
			foreach ($post_meta_infos as $meta_info) {
				$meta_key = $meta_info->meta_key;
				$meta_value = addslashes($meta_info->meta_value);
				$sql_query_sel[]= "SELECT $new_post_id, '$meta_key', '$meta_value'";
			}
			$sql_query.= implode(" UNION ALL ", $sql_query_sel);
			$wpdb->query($sql_query);
		}
 
 
		/*
		 * finally, redirect to the edit post screen for the new draft
		 */
		wp_redirect( admin_url( 'post.php?action=edit&post=' . $new_post_id ) );
		exit;
	} else {
		wp_die('Post creation failed, could not find original post: ' . $post_id);
	}
}
add_action( 'admin_action_rd_duplicate_post_as_draft', 'rd_duplicate_post_as_draft' );
 
/*
 * Add the duplicate link to action list for post_row_actions
 */
function rd_duplicate_post_link( $actions, $post ) {
	if (current_user_can('edit_posts')) {
		$actions['duplicate'] = '<a href="admin.php?action=rd_duplicate_post_as_draft&amp;post=' . $post->ID . '" title="Duplicate this item" rel="permalink">Duplicate</a>';
	}
	return $actions;
}


/*----------------------------------------------------*
| ALL VIDEOS RESPONSIVE                               |
*----------------------------------------------------*/

add_action ( 'wp_enqueue_scripts', 'rgc_fluidvid' );

function rgc_fluidvid() {
  	$fluidURL = get_stylesheet_directory_uri() . '/js/fluidvids.min.js';
		wp_enqueue_script( 'fluidvids', $fluidURL, array(), false, true ); 
}


/*----------------------------------------------------*
| MAINTENANCE MODE                                    |
*----------------------------------------------------*/
//IN PROGRESS

/*

function wpse_form_in_admin_bar() {
    global $wp_admin_bar;

	echo '<style type="text/css">input#maint_mode_on {display: none;} input#maint_mode_on:checked ~ #maintLabel {color: red;}</style>';

  	$mainActivated = true;
  
    $wp_admin_bar->add_menu( array(
        'id' => 'maint-mode-button',
        'parent' => 'top-secondary',
        'title' => '<input id="maint_mode_on" type="checkbox" name="maint_mode_on" value="$mainActivated"><label id="maintLabel" for="maint_mode_on">Maintenance Mode</label>'
    ) );
}
add_action( 'admin_bar_menu', 'wpse_form_in_admin_bar' );


// add maintenance mode
function lcs_maint_mode_on() {
  
    if ( !current_user_can( 'edit_themes' ) || !is_user_logged_in()) {
	  
	  $stylesheet = get_stylesheet_directory_uri();
	  $styles = '<style type="text/css">#maintLogo {padding-top: 30px;text-align: center;}#maintCenter {text-align: center;}p#maintText {font-size: 20px;padding-top: 20px;text-align: center;}</style>';
	  $firsthalf = '<div id="maintWrap"><div id="maintLogo">';
	  $mid = '<img src="' . $stylesheet . '/images/client_logo.png' . '">';
	  $lasthalf = '</div><p id="maintText">This website is currently being maintained. We apologize for the inconvenience.<br /> <strong>Please try back again in a few minutes.<strong></p></div>';
	  
	  echo $styles . $firsthalf . $mid . $lasthalf;
	  
	  die();
    }
}
add_action('get_header', 'lcs_maint_mode_on');
*/


/*----------------------------------------------------*
| INCLUDE EXTERNAL FILE SHORTCODE                     |
*----------------------------------------------------*/	

function show_file_func( $atts ) {
  extract( shortcode_atts( array(
    'file' => ''
  ), $atts ) );
 
  if ($file!='')
    return @file_get_contents(get_stylesheet_directory_uri() . "/" . $file);
}

add_shortcode( 'show_file', 'show_file_func' );


/*----------------------------------------------------*
| DON'T SHOW AUTHOR USER NAMES                        |
*----------------------------------------------------*/	

function bwp_author_redirect() {
	if (is_author()) {
		wp_redirect( home_url() ); exit;
	}
}

add_action('template_redirect', 'bwp_author_redirect');


/*----------------------------------------------------*
| DON'T SHOW AUTHOR USER NAMES                        |
*----------------------------------------------------*/	

add_action('admin_footer','posts_status_color');
      function posts_status_color(){
    ?>
    <style>
      .status-draft .check-column {box-shadow: -12px 0 0 -3px #fa7184 !important;}
     .status-pending .check-column {box-shadow: -12px 0 0 -3px #ffcc53 !important;}
     .status-publish .check-column {box-shadow: -12px 0 0 -3px #88f091 !important;}
      .status-future .check-column {box-shadow: -12px 0 0 -3px #ffffff !important;}
     .status-private .check-column {box-shadow: -12px 0 0 -3px #000000 !important;}
    </style>
    <?php
}


/*----------------------------------------------------*
| DON'T SHOW AUTHOR USER NAMES                        |
*----------------------------------------------------*/	

function complete_version_removal() {
        return '';
    }
add_filter('the_generator', 'complete_version_removal');


/*----------------------------------------------------*
| FORCE SHARPER THUMBNAIL IMAGES                      |
*----------------------------------------------------*/	

function ajx_sharpen_resized_files( $resized_file ) {
 
        $image = wp_load_image( $resized_file );
        if ( !is_resource( $image ) )
            return new WP_Error( 'error_loading_image', $image, $file );
     
        $size = @getimagesize( $resized_file );
        if ( !$size )
            return new WP_Error('invalid_image', __('Could not read image size'), $file);
        list($orig_w, $orig_h, $orig_type) = $size;
     
        switch ( $orig_type ) {
            case IMAGETYPE_JPEG:
                $matrix = array(
                    array(-1, -1, -1),
                    array(-1, 16, -1),
                    array(-1, -1, -1),
                );
     
                $divisor = array_sum(array_map('array_sum', $matrix));
                $offset = 0;
                imageconvolution($image, $matrix, $divisor, $offset);
                imagejpeg($image, $resized_file,apply_filters( 'jpeg_quality', 90, 'edit_image' ));
                break;
            case IMAGETYPE_PNG:
                return $resized_file;
            case IMAGETYPE_GIF:
                return $resized_file;
        }
     
        return $resized_file;
    }  
     
    add_filter('image_make_intermediate_size', 'ajx_sharpen_resized_files',900);
    
 
 /*----------------------------------------------------*
| REMOVE YOAST ANALYSIS CRAP TRAIL                   |
*----------------------------------------------------*/
   
add_filter( 'wpseo_use_page_analysis', '__return_false' );

add_action('admin_init', 'remove_yoast_stoplight', 11);
function remove_yoast_stoplight(){
    add_filter( 'wpseo_use_page_analysis', '__return_false' );
    global $wpseo_metabox;
    if($wpseo_metabox) remove_action( 'post_submitbox_start', array( $wpseo_metabox, 'publish_box' ) );
}


 /*----------------------------------------------------*
| USE EMAIL FOR LOGIN                                 |
*----------------------------------------------------*/

add_filter('authenticate', function($user, $username, $password)
{
    if (is_email($username))
    {
        $user = get_user_by('email', $username);
        if($user)
            $username = $user->user_login;
    }
    return wp_authenticate_username_password(null, $username, $password);
}, 20, 3);

function change_username_wps_text($text){
       if(in_array($GLOBALS['pagenow'], array('wp-login.php'))){
         if ($text == 'Username'){$text = 'Username / Email';}
            }
                return $text;
         }
add_filter( 'gettext', 'change_username_wps_text' );


 /*----------------------------------------------------*
| FACEBOOK THUMBNAIL META                            |
*----------------------------------------------------*/

function insert_image_src_rel_in_head() {
	global $post;
	if ( !is_singular()) //if it is not a post or a page
		return;
	if(!has_post_thumbnail( $post->ID )) { //the post does not have featured image, use a default image
		$default_image = get_stylesheet_directory_uri() . "images/client_logo.png"; //replace this with a default image on your server or an image in your media library
		echo '<meta property="og:image" content="' . $default_image . '"/>';
	}
	else{
		$thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );
		echo '<meta property="og:image" content="' . esc_attr( $thumbnail_src[0] ) . '"/>';
	}
	echo "
";
}
add_action( 'wp_head', 'insert_image_src_rel_in_head', 5 );


 /*----------------------------------------------------*
| RETURN THUMBNAIL URL                              |
*----------------------------------------------------*/

function thumbnail_url($size) {
	$thumb_id = get_post_thumbnail_id();
	$thumb_url = wp_get_attachment_image_src($thumb_id, $size, true);

	return $thumb_url[0];
}


 /*----------------------------------------------------*
| ALLOW UPLOAD OF SVG                                  |
*----------------------------------------------------*/
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');
  
