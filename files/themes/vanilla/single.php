<?php
/**
 * Single Template
 * @package vanilla - an adaptation from the theme Frank
 */
?>
<?php get_header(); ?>
<main id="theContent">
	<!--the loop-->
	<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : ?>
	<?php the_post(); ?>
			
	<?php include_once('includes/content.php'); ?><!--pull content from content.php-->
			
	<?php endwhile; endif; ?>
	<?php comments_template(); ?>
</main><!--end #theContent-->
<?php get_footer(); ?>