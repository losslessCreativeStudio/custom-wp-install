<?php
/**
 * Index Template
 * @package vanilla - an adaptation from the theme Frank
 */
?>
<?php get_header(); ?>
<main id="theContent">
	<!--the loop-->
	<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : ?>
	<?php the_post(); ?>
			
	<!--HOMEPAGE stuff here, like a slider or blog posts or widgets-->
			
	<?php endwhile; endif; ?>
	<?php comments_template(); ?>
</main><!--end #theContent-->
<?php get_footer(); ?>