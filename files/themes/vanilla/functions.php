<?php
// Exit if accessed directly
if ( !defined('ABSPATH')) exit;


/*----------------------------------------------------*
| LOAD CUSTOM ADMIN STYLESHEET                        |
*----------------------------------------------------*/

function load_custom_wp_admin_style() {
        wp_register_style( 'custom_wp_admin_css', get_stylesheet_directory_uri() . '/admin-style.css', false, '1.0.0' );
        wp_enqueue_style( 'custom_wp_admin_css' );
}

add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );


/*----------------------------------------------------*
| LOAD CUSTOM LOGIN LOGO                              |
*----------------------------------------------------*/

function custom_login_logo() { ?>
    <style type="text/css">
        body.login div#login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/client_logo.png);
            height: 100px;
            background-size: auto;
            width: auto;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'custom_login_logo' );

function my_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

function my_login_logo_url_title() {
    return 'Your Site Name and Info';
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );
	
	
/*----------------------------------------------------*
| ADD FAVICON                                         |
*----------------------------------------------------*/



function my_favicon() { ?>
	<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.png" >
<?php }
	add_action('wp_head', 'my_favicon');


/*----------------------------------------------------*
| SHOW FIRST CATEGORY FILED UNDER                     |
*----------------------------------------------------*/

function first_category() {
	$category = get_the_category(); 
	echo $category[0]->cat_name;
}
//put first_category() in a template to use.





function first_taxonomy() {
	$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); 
	echo $term->name;
}
//put first_taxonomy() in a template to use.





/*----------------------------------------------------*
| REMOVE DEFAULT IMG LINKS                            |
*----------------------------------------------------*/	

function wpb_imagelink_setup() {
	$image_set = get_option( 'image_default_link_type' );
	
	if ($image_set !== 'none') {
		update_option('image_default_link_type', 'none');
	}
}
add_action('admin_init', 'wpb_imagelink_setup', 10);


/*----------------------------------------------------*
| PROCESS AND VALIDATE CUSTOM FIELD URLS              |
*----------------------------------------------------*/	


function url($value) {
  	//strip the url
 	$url = str_replace('http://', '', $value);
  
  	//sanitize URL
  	$url = filter_var($url, FILTER_SANITIZE_URL);
  	$url = 'http://' . $url . "/";
  
  	//check that it is a valid URL and has the following TLDs
  	if (!filter_var($url, FILTER_VALIDATE_URL, FILTER_FLAG_HOST_REQUIRED) === false && preg_match("/.com/i", $url) || preg_match("/.net/i", $url) || preg_match("/.org/i", $url) || preg_match("/.co/i", $url) || preg_match("/.us/i", $url) || preg_match("/.io/i", $url) || preg_match("/.ca/i", $url) || preg_match("/.uk/i", $url) || preg_match("/.edu/i", $url)  ) {
    return $url;
	} else {
    return '#';
	}
}

// use url('YOUR URL') to use this - make sure the TLD is in here


/*----------------------------------------------------*
| Remove p tags from WordPress img                    |
*----------------------------------------------------*/	

function filter_ptags_on_images($content){
   return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}

add_filter('the_content', 'filter_ptags_on_images');
