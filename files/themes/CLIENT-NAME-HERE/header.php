<?php
/**
* @package vanilla - an adaptation from the theme Frank
*/
?>

<!DOCTYPE html>
<!--[if IE 7 | IE 8]>
<html class="ie" lang="en-US">
<![endif]-->
<!--[if (gte IE 9) | !(IE)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>

	<!-------------------
    | META
    '------------------->
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	
	<!--Safari 9 pinability-->
	<link rel="icon" sizes="any" mask href="<?php echo get_stylesheet_directory_uri(); ?>/images/pin.svg">
	<meta name="theme-color" content="#fff"><!--add pin color here-->
	
	<!-------------------
    | STYLESHEETS
    '------------------->
	<link rel='stylesheet' href="<?php echo get_stylesheet_directory_uri(); ?>/style.min.css" type='text/css' />
	
	<!-------------------
    | SCRIPTS
    '------------------->
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/prefixfree.min.js"></script>
	<!--<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/functions.js"></script>-->
	<!--<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.base.min.js"></script>-->
	<!--<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/fluidvids.min.js"></script>-->
	<!--<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/smoothscroll.min.js"></script>-->
	<!--<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/match.js"></script>-->

	 <!-------------------
    | GOOGLE ANALYTICS
    '------------------->
    	<!--Put Google Analytics Tracking Code Here-->
    	
    <!-------------------
    | ADOBE TYPEKIT
    '------------------->
    	<!--Put Typekit Snippet here if using-->

	<!-------------------
    | WORDPRESS HOOKS
    '------------------->
	<?php wp_head(); ?>

</head>

<body id="screen" <?php body_class(); ?>>
		<header id="theHeader">
		  <!--HEADER stuff here like a nav or logo-->
		</header>
