Customization Checklist

**Images in use by theme:**

	FAVICON                                 images/favicon.png
	LOGO (Dashboard, Admin Login)           images/client_logo.png

**Files to modify:**

	THEME NAME/ INFO                        SCSS/_partials/_wp-info.scss        
	THEME DIRECTORY SLUG                    ../CLIENT-NAME-HERE       
	THEME SCREENSHOT                        screenshot.png
	PROJECT NAME/ INFO                      SCSS/package.son
	ADD MORE SCSS PARTIALS                  SCSS/_partials && SCSS/master.scss (compile order)
	DEFAULT MAIN CSS (SASSY)                SCSS/_partials/_main.scss
	DEFAULT MIXIN & VARIABLES CSS (SASSY)   SCSS/_partials/_mixins.scss
	ADMIN STYLES                            admin-style.css