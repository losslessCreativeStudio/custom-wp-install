function matchHeight() {
    var inners = document.getElementsByClassName("match");
    var maxHeight = 0;
    for (var i = 0; i < inners.length; i++) {
        if (inners[i].offsetHeight > maxHeight)
            maxHeight = inners[i].offsetHeight;
    }
    for (var i = 0; i < inners.length; i++) {
      inners[i].style.height = maxHeight + 'px';
    }
  }

  var updateAndMatch = debounce(function() {

    var inners = document.getElementsByClassName("match");
    var maxHeight = 0;

    /** reset heights ***/
    for (var i = 0; i < inners.length; i++) {
      inners[i].style.height = "auto";
    }

    for (var i = 0; i < inners.length; i++) {
        if (inners[i].offsetHeight > maxHeight)
            maxHeight = inners[i].offsetHeight;
    }
    for (var i = 0; i < inners.length; i++) {
        inners[i].style.height = maxHeight + 'px';
      }
  }, 50, true);

document.addEventListener('DOMContentLoaded', matchHeight);
window.addEventListener('resize', updateAndMatch);
