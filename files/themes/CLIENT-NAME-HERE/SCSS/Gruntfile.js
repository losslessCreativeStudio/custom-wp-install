module.exports = function(grunt) {

    grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		/**
		 * Sass
		 */
		sass: {
		  dev: {
		    options: {
		      style: 'expanded',
		      sourcemap: 'none',
		    },
		    files: {
		      '../style.css': 'master.scss'
		    }
		  }
		},
        
        /**
        * Minify
        */
        cssmin : {
            css:{
                src: '../style.css',
                dest: '../style.min.css'
            }
        }, 

	  	/**
	  	 * Watch
	  	 */
		watch: {
			css: {
				files: '_partials/*.scss',
				tasks: ['sass', 'cssmin']
			}
		},

	});
	grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.registerTask('default',['watch']);
}